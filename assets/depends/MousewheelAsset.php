<?php

namespace app\assets\depends;

use yii\web\AssetBundle;


class MousewheelAsset extends AssetBundle
{
    public $sourcePath = "@bower/jquery-mousewheel";
    public $css = [];
    public $js = [
        "jquery.mousewheel.min.js",
    ];
    public $depends = [
        "yii\\web\\YiiAsset"
    ];
}
