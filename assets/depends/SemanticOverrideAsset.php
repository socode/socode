<?php

namespace app\assets\depends;

use yii\web\AssetBundle;


class SemanticOverrideAsset extends AssetBundle
{
    public $basePath = "@webroot";
    public $baseUrl = "@web";
    public $css = [
        "less/semantic/semantic_override.less",
    ];
}
