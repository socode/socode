<?php

namespace app\assets\depends;

use yii\web\AssetBundle;


class VelocityAsset extends AssetBundle
{
    public $sourcePath = "@bower/velocity";
    public $css = [];
    public $js = [
        "velocity.min.js",
    ];
    public $depends = [
        "yii\\web\\YiiAsset"
    ];
}
