<?php

namespace app\assets\depends;

use yii\web\AssetBundle;


class ParticleAsset extends AssetBundle
{
    public $sourcePath = "@vendor/bower/particles.js";
    public $css = [];
    public $js = [
        "particles.min.js",
    ];
    public $depends = [
        "yii\\web\\YiiAsset"
    ];
}
