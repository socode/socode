<?php

namespace app\assets\depends;

use yii\web\AssetBundle;


class InputmaskAsset extends AssetBundle
{
    public $sourcePath = "@bower/jquery.inputmask";
    public $css = [];
    public $js = [
        "dist/min/jquery.inputmask.bundle.min.js",
    ];
    public $depends = [
        "yii\\web\\YiiAsset"
    ];
}
