<?php

namespace app\assets\depends;

use yii\web\AssetBundle;


class SemanticAsset extends AssetBundle
{
    public $sourcePath = "@vendor/bower/semantic/";
    public $css = [
        "dist/semantic.min.css",
    ];
    public $js = [
        "dist/semantic.min.js",
        "dist/components/accordion.min.js",
    ];
    public $depends = [
        "yii\\web\\YiiAsset"
    ];
}
