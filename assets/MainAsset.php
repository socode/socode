<?php

namespace app\assets;

use yii\web\AssetBundle;


class MainAsset extends AssetBundle
{
    public $basePath = "@webroot";
    public $baseUrl = "@web";
    public $css = [
        "//fonts.googleapis.com/css?family=PT+Sans:400,400i,700",
        "less/semantic/semantic_override.less",
        "less/main_build.less",
    ];
    public $js = [
        "js/main.js",
        "js/events.js",
        "js/scroll.js",
        "js/mail.js"
    ];
    public $depends = [
        "yii\\web\\YiiAsset",
        "app\\assets\\depends\\SemanticAsset",
        "app\\assets\\depends\\SemanticOverrideAsset",
        "app\\assets\\depends\\ParticleAsset",
        "app\\assets\\depends\\MousewheelAsset",
        "app\\assets\\depends\\InputmaskAsset",
        "app\\assets\\depends\\VelocityAsset",
    ];
}

