<?php
return [
    'name' => 'Socode',
    'name_en' => 'Socode',
    'title' => 'Socode.ru | Full-stack разработчик Станислав Зубин',
    'description' => 'Фрилансер и Full-stack разработчик, реализует высоконагруженные системы.',
    'keywords' => 'Full-stack разработчик, Full-stack разработка, веб дизайн, php программист, yii программист',

    'about_name' => 'Станислав Зубин',
    'profession' => 'Full-stack разработчик',
    'write' => 'Написать',
];