<?php
return [
    'tm' => 'ЗАО Тяжмаш',
    'tm_position' => 'Инженер-программист',
    'tm_description' => 'Внутренний сайт предприятия',

    'ac' => 'Artcream',
    'ac_position' => 'Программист',
    'ac_description' => 'Проект онлайн стрим TV Zobr',

    'xc' => 'Xcraft',
    'xc_position' => 'Программист (Фриланс)',
    'xc_description' => 'Личный кабинет пользователей, внутренние механизмы',

    'inf' => 'InfoGis',
    'inf_position' => 'Программист',
    'inf_description' => 'Корпоративный стиль, сайты для клиентов',

    'mif' => 'Издательство МИФ',
    'mif_position' => 'Программист (Фриланс)',
    'mif_description' => 'Внутренние части сайта',

    'noo' => 'Noosphere',
    'noo_position' => 'Программист (Фриланс)',
    'noo_description' => 'Внутренние части сайта',

    'artec' => 'Artec',
    'artec_position' => 'Back-end программист',
    'artec_description' => 'Web часть стартапа',

    'work_process' => 'Загрузка',
    'works' => [
        '1' => [
            'work_header' => 'ЗАО Тяжмаш',
            'work_image' => '/img/works/tm.png',
            'work_position' => 'Инженер-программист',
            'work_technology' => 'Внутренний сайт предприятия',
            'work_description' => 'Внутренний сайт предприятия',
        ],
        '2' => [
            'work_header' => 'Artcream',
            'work_image' => '/img/works/zobr.png',
            'work_position' => 'Программист',
            'work_technology' => 'Проект онлайн стрим TV Zobrа',
            'work_description' => 'Проект онлайн стрим TV Zobrа',
        ],
        '3' => [
            'work_header' => 'Xcraft',
            'work_image' => '/img/works/xcraft.png',
            'work_position' => 'Программист (Фриланс)',
            'work_technology' => 'Личный кабинет пользователей, внутренние механизмы',
            'work_description' => 'Личный кабинет пользователей, внутренние механизмы',
        ],
        '4' => [
            'work_header' => 'InfoGis',
            'work_image' => '/img/works/infogis.png',
            'work_position' => 'Программист',
            'work_technology' => 'Корпоративный стиль, сайты для клиентов',
            'work_description' => 'Корпоративный стиль, сайты для клиентов',
        ],
        '5' => [
            'work_header' => 'Издательство МИФ',
            'work_image' => '/img/works/mif.png',
            'work_position' => 'Программист (Фриланс)',
            'work_technology' => 'Внутренние части сайта',
            'work_description' => 'Внутренние части сайта',
        ],
        '6' => [
            'work_header' => 'Noosphere',
            'work_image' => '/img/works/noo.png',
            'work_position' => 'Программист (Фриланс)',
            'work_technology' => 'Внутренние части сайта',
            'work_description' => 'Внутренние части сайта',
        ],
        '7' => [
            'work_header' => 'Artec',
            'work_image' => '/img/works/artec.png',
            'work_position' => 'Back-end программист',
            'work_technology' => 'Web часть стартапа',
            'work_description' => 'Web часть стартапа',
        ],

    ]
];