<?php
return [
    'main' => 'Главная',
    'works' => 'Работы',
    'clients' => 'Клиенты',
    'contacts' => 'Контакты',
    'about' => 'Обо мне',
    'menu' => 'Меню',
];