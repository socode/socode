<?php
return [
    'name' => 'Socode',
    'name_en' => 'Socode',
    'title' => 'Socode.ru | Full-stack developer Stanislav Zubin',
    'description' => 'Freelancer and Full-stack developer, realize high-load system.',
    'keywords' => 'Full-stack developer, Full-stack develop, wed design, php programmer, yii programmer',

    'about_name' => 'Stanislav Zubin',
    'profession' => 'Full-stack developer',
    'write' => 'Write',
];