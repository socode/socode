<?php
return [
    'tm' => 'ZAO Tyazhmash',
    'tm_position' => 'Software Engineer',
    'tm_description' => 'Internal site of the enterprise',

    'ac' => 'Artcream',
    'ac_position' => 'Programmer',
    'ac_description' => 'Project online TV "TV Zobr"',

    'xc' => 'Xcraft',
    'xc_position' => 'Programmer (Freelance)',
    'xc_description' => 'User\'s personal cabinet, internal mechanisms',

    'inf' => 'InfoGis',
    'inf_position' => 'Programmer',
    'inf_description' => 'Corporate style, sites for clients',

    'mif' => 'Publishing house MYTH',
    'mif_position' => 'Programmer (Freelance)',
    'mif_description' => 'Internal parts of the site',

    'noo' => 'Noosphere',
    'noo_position' => 'Programmer (Freelance)',
    'noo_description' => 'Internal parts of the site',

    'artec' => 'Artec',
    'artec_position' => 'Back-end programmer',
    'artec_description' => 'Web part of the startup',

    'work_process' => 'Loading',
    'works' => [
        '1' => [
            'work_header' => 'ZAO Tyazhmash',
            'work_image' => '/img/works/tm.png',
            'work_position' => 'Software Engineer',
            'work_technology' => 'Internal site of the enterprise',
            'work_description' => 'Internal site of the enterprise',
        ],
        '2' => [
            'work_header' => 'Artcream',
            'work_image' => '/img/works/zobr.png',
            'work_position' => 'Programmer',
            'work_technology' => 'Project online TV "TV Zobr"',
            'work_description' => 'Project online TV "TV Zobr"',
        ],
        '3' => [
            'work_header' => 'Xcraft',
            'work_image' => '/img/works/xcraft.png',
            'work_position' => 'Programmer (Freelance)',
            'work_technology' => 'User\'s personal cabinet, internal mechanisms',
            'work_description' => 'User\'s personal cabinet, internal mechanisms',
        ],
        '4' => [
            'work_header' => 'InfoGis',
            'work_image' => '/img/works/infogis.png',
            'work_position' => 'Programmer',
            'work_technology' => 'Corporate style, sites for clients',
            'work_description' => 'Corporate style, sites for clients',
        ],
        '5' => [
            'work_header' => 'Издательство МИФ',
            'work_image' => '/img/works/mif.png',
            'work_position' => 'Programmer (Freelance)',
            'work_technology' => 'Internal parts of the site',
            'work_description' => 'Internal parts of the site',
        ],
        '6' => [
            'work_header' => 'Noosphere',
            'work_image' => '/img/works/noo.png',
            'work_position' => 'Programmer (Freelance)',
            'work_technology' => 'Internal parts of the site',
            'work_description' => 'Internal parts of the site',
        ],
        '7' => [
            'work_header' => 'Artec',
            'work_image' => '/img/works/artec.png',
            'work_position' => 'Back-end programmer',
            'work_technology' => 'Web part of the startup',
            'work_description' => 'Web part of the startup',
        ],

    ]
];