<?php
return [
    "site_is_offline" => "This site is down for maintenance. Sorry for the inconvenience!",
    "error_turn_back" => "Turn back",
    "error_page" => "It seems there was an error. Sorry about that. :("
];