<?php
return [
    'about_part_one' => 'Greetings! My name is Stanislav. I\'m a full-stack developer. Web-development began to be interested
                    from the age of 17. At 19, a couple of friends organized a small web-studio. Installed on CMS Joomla
                    small sites and online stores. After school I entered the Polytechnic College, majoring in Computer
                    Science. He defended his diploma perfectly and became a "technician". At the pair all with the same
                    friend, got a job as a software engineer. There he received his first programming experience. At the
                    plant, I developed the internal site of the enterprise and interactive presentations. Invented
                    "bicycles", from which, for lack of experience, I enjoyed myself immensely. In parallel with his
                    work, he entered the SF SamSTU for correspondence faculty, specializing in "Computers, complexes,
                    systems and networks." He worked at the plant for 2 years and rose to the engineer-programmer. Then
                    I decided to move to Samara.',
    'about_part_two' => 'No sooner said than done. In Samara, being a student, he passed an internship in a web studio. In
                    the process, the studio decided to move to St. Petersburg in connection with which it was necessary
                    to leave it. After that, it was a pleasure to do freelancing. In parallel with freelance I passed
                    the state, wrote a diploma, and defended it perfectly, Becoming thus the "Engineer". After
                    graduation he continued to work remotely. Was engaged in the development of large and not
                    very Projects, sites, and their parts. During this time, a large number of problems and tasks were
                    solved. The last and most interesting project I was working on was a startup to work with the
                    Additional Reality. Which allows to solve, or at least optimize A lot of problems for the market for
                    the sale of furniture and various large goods.',
];