<?php
return [
    'main' => 'Home',
    'works' => 'Works',
    'clients' => 'Clients',
    'contacts' => 'Contacts',
    'about' => 'About',
    'menu' => 'Menu',
];