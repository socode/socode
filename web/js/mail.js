$(document).ready(function () {
    "use strict";

    /** Отправка сообщения */
    var write_me_form = $("#write-me-form"),
        contact_name = $("#contact-name"),
        contact_phone = $("#contact-phone"),
        contact_message = $("#contact-message"),
        csrf_contact = $("#csrf-contact").val($('meta[name="csrf-token"]').attr("content")),
        send_contact = $("#send-contact");

    write_me_form.on("submit", function (e) {
        e.preventDefault();
        $.ajax({
            url: this.action,
            type: 'post',
            dataType: 'json',
            data: write_me_form.serialize(),
            success: function (response) {
                console.log(response);
                return false;
            }
        });

    });
});