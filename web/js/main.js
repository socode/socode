$(document).ready(function () {
    "use strict";

    /** Базовые функции */
    /** Возвращает случайное значение от min до max
     * @param min
     * @param max
     * @returns {*}
     */
    var getRandom = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };

    /**
     * Базовая инициализация элементов
     * @type {{init: init}}
     */
    var BaseInit = {
        /** Инициализация */
        init: function () {
            /** Активация дропдаун меню */
            $(".ui.dropdown").dropdown();

            /** Маска для инпута */
            $("#contact-phone").inputmask("+7(###)###-##-##");
            $("#contact-email").inputmask("email");
        }
    };

    /**
     * Объект галереи с частицами
     * @type {{file_config_number: string, setPartJsConfigNumber, getPartJsConfigNumber: number, init: init}}
     */
    var RightGallery = {
        /** Номер файла конфига для частиц */
        file_config_number: "undefined",

        /**
         * Задаёт номер конфига для эфекта частиц
         * @param int_value
         */
        set setPartJsConfigNumber(int_value) {
            this.file_config_number = int_value;
        },

        /**
         * Получвает номер конфига для эфекта частиц
         * @returns {number}
         */
        get getPartJsConfigNumber() {
            return Number(typeof this.file_config_number === "undefined" ? 1 : this.file_config_number);
        },

        /**
         * Возвращает позицию для CSS по ID
         * @param pos_id_int
         * @returns {*}
         */
        getPosition: function (pos_id_int) {
            switch (pos_id_int) {
                case 1:
                    return "left";
                case 2:
                    return "center";
                case 3:
                    return "right";
                default:
                    return "left";
            }
        },

        /**
         * Задаёт случайную картинку и её позицию для правого блока
         * @param jq_obj
         * @param rand_img_int
         * @param rand_pos_int
         * @returns {*}
         */
        setRandomBgImg: function (jq_obj, rand_img_int, rand_pos_int) {
            var position = this.getPosition(rand_pos_int);
            return jq_obj.css({
                "background": "url(img/side_img_" + rand_img_int + ".jpg) center " + position + " / cover"
            });
        },

        /** Инициализация */
        init: function () {
            this.setPartJsConfigNumber = getRandom(1, 5);
            var particle_config_name = "particlesjs-config-" + this.getPartJsConfigNumber + ".json";
            particlesJS.load('particles-js', './js/particle-json/' + particle_config_name);

            /** Задаём боковую картинку случайным образом */
            var side_img_block = $(".side-image-block"), // Контейнер под картинку
                random_image_number = getRandom(1, 3), // Случайный номер картинки
                random_position_number = getRandom(1, 3); // Случайная позиция картинки

            this.setRandomBgImg(side_img_block, random_image_number, random_position_number);
        }
    };


    /** Инициализация объектов */
    BaseInit.init();
    RightGallery.init();
});