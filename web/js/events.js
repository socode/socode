$(document).ready(function () {
    "use strict";

    /** Мобильное меню */
    $("#mobile-menu-btn").on("click", function () {
        var body = $("body"),
            sidebar = $(".sidebar");

        /** Убирает возможность скролить при открытом сайдбаре */
        body.css("overflow", "hidden");

        sidebar.sidebar({
            "onHide": function () {
                /** Возвращает скрол после закрытия сайдбара */
                body.css("overflow", "");
            }
        }).sidebar("show");
    });

    /** Действие кнопки написать мне | скролл до формы и фокус на инпут имени */
    $(".write-me-btn").on("click", function () {
        $("#write-me-form").velocity("scroll", {
            duration: 500,
            mobileHA: false,
            easing: 'ease-in-out',
            complete: function () {
                $("#contact-name").focus();
            }
        });
    });

    /** Модальные окна работ */
    $(".work-card").on("click", function () {

        var work_id = $(this).data("work-id"),
            work_modal = $(".work-modal"),
            work_header = $(".work-modal .work-header"),
            work_process = $(".work-modal .work-process"),
            work_image = $(".work-modal .work-image"),
            work_position = $(".work-modal .work-position"),
            work_technology = $(".work-modal .work-technology"),
            work_description = $(".work-modal .work-description"),
            url = work_modal.attr("data-url"); // URL к апи

        // Включаем загрузчик
        var dimmer = work_modal.find(".dimmer").addClass("active");
        $.ajax({
            url: url,
            type: "GET",
            data: {"id": work_id},
            success: function (response) {
                if (typeof response === "object" &&
                    response.status === 200) {

                    work_header.html(response.work.work_header); // Название организации
                    work_process.html(response.work_process); // Перевод для диммера
                    work_image.attr("src", response.work.work_image); // Ссылка на картинку
                    work_position.html(response.work.work_position); // Занимаемая должность
                    work_technology.html(response.work.work_technology); // Список технологий
                    work_description.html(response.work.work_description); // Описание проекта

                    work_modal.modal("show");
                    dimmer.removeClass('active');

                    return false;
                } else {

                    dimmer.removeClass('active');
                    return false;
                }
            },
            error: function () {
                dimmer.removeClass('active');
                return false;
            }
        });
    })

});