$(document).ready(function () {
    "use strict";

    /** Скролл до блоков */
    var active_nav_objects = $(".active-navigation");
    active_nav_objects.on("click", function () {
        $($(this).data("scrollto")).velocity("scroll", {
            //duration: 500,
            /*mobileHA: false,
             easing: 'ease-in-out',
             queue: false,*/
            complete: function () {
                /*if ($(".sidebar").sidebar("is visible")) {
                 $(".sidebar").sidebar("hide");
                 }*/
            }
        });
    });

    var active_nav_objects1 = $(".sidebar-active-navigation");
    active_nav_objects1.on("click", function () {
        $($(this).data("scrollat")).velocity("scroll");
    });

    /** Обработчик событий прокрутки блока клиентов + фикс для тач эвентов */
    $('#clients')
        .on('mousewheel', ".work-with-container", function (event, delta) {
            this.scrollLeft -= (delta * 30);
            event.preventDefault();
        })
        .on("touchstart", ".work-with-container", function (e) {
            var startingX = e.originalEvent.touches[0].pageX;

            $(this).on("touchmove", function (e) {
                var currentX = e.originalEvent.touches[0].pageX;
                var delta = currentX - startingX;
                this.scrollLeft -= delta / 10;

            });

            $(this).on("touchend", function (e) {
                $(this).off("touchmove");
            })
        });
});