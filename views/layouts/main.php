<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\widgets\WLang;
use app\assets\MainAsset;

MainAsset::register($this);

$this->title = Yii::t('appMain', 'title');
$description = Yii::t('appMain', 'description');
$keywords = Yii::t('appMain', 'keywords');

?>

<?php $this->beginPage(); ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language; ?>">
    <head>
        <meta charset="<?= Yii::$app->charset; ?>">
        <title><?= Html::encode($this->title); ?></title>

        <meta name="description" content="<?= Html::encode($description); ?>">
        <meta name="keywords" content="<?= Html::encode($keywords); ?>">

        <meta name="viewport"
              content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=0, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <?= Html::csrfMetaTags(); ?>

        <meta name="author" content="www.socode.ru">
        <meta name="revisit-after" content="1 days">
        <meta name="HandheldFriendly" content="True">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="robots" content="index, follow">

        <meta property="og:site_name" content="Socode"/>
        <meta property="og:title" content="<?= Html::encode($this->title); ?>"/>
        <meta property="og:description" name="description" content="<?= Html::encode($description); ?>"/>
        <meta property="og:type" content="website"/>
        <meta property="og:url" content="<?= Yii::$app->request->hostInfo; ?>"/>
        <meta property="og:image" content="<?= Yii::$app->request->hostInfo . Url::to(["img/icon/ico512.png"]); ?>"/>

        <link href="<?= Url::to(["img/icon/favicon.ico"]); ?>" rel="icon" type="image/x-icon">

        <link rel="apple-touch-icon" sizes="57x57" type="image/png" href="<?= Url::to(["img/icon/ico57.png"]); ?>">
        <link rel="apple-touch-icon" sizes="60x60" type="image/png" href="<?= Url::to(["img/icon/ico60.png"]); ?>">
        <link rel="apple-touch-icon" sizes="72x72" type="image/png" href="<?= Url::to(["img/icon/ico72.png"]); ?>">
        <link rel="apple-touch-icon" sizes="76x76" type="image/png" href="<?= Url::to(["img/icon/ico76.png"]); ?>">
        <link rel="apple-touch-icon" sizes="114x114" type="image/png" href="<?= Url::to(["img/icon/ico114.png"]); ?>">
        <link rel="apple-touch-icon" sizes="120x120" type="image/png" href="<?= Url::to(["img/icon/ico120.png"]); ?>">
        <link rel="apple-touch-icon" sizes="144x144" type="image/png" href="<?= Url::to(["img/icon/ico144.png"]); ?>">
        <link rel="apple-touch-icon" sizes="152x152" type="image/png" href="<?= Url::to(["img/icon/ico152.png"]); ?>">
        <link rel="apple-touch-icon" sizes="180x180" type="image/png" href="<?= Url::to(["img/icon/ico180.png"]); ?>">

        <link rel="icon" type="image/png" href="<?= Url::to(["img/icon/ico16.png"]); ?>" sizes="16x16">
        <link rel="icon" type="image/png" href="<?= Url::to(["img/icon/ico32.png"]); ?>" sizes="32x32">
        <link rel="icon" type="image/png" href="<?= Url::to(["img/icon/ico96.png"]); ?>" sizes="96x96">
        <link rel="icon" type="image/png" href="<?= Url::to(["img/icon/ico192.png"]); ?>" sizes="192x192">

        <?= WLang::widget(["meta_alt_only" => true]); ?>

        <?php $this->head(); ?>

        <!--[if lte IE 9]>
        <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.1/html5shiv.js"></script>
        <![endif]-->
        <!-- ♥ -->

    </head>
    <body>

    <?php

    $this->beginBody(); ?>

    <!-- Мобильное меню -->
    <?= Yii::$app->controller->renderPartial("menu/sidebar"); ?>

    <div class="ui grid pusher">

        <div class="twelve wide computer sixteen wide mobile column left-content-side">

            <!-- Основное меню -->
            <?= Yii::$app->controller->renderPartial("menu/main"); ?>

            <!-- Раздел Главная -->
            <?= Yii::$app->controller->renderPartial("parts/main"); ?>

            <!-- Раздел Работы -->
            <?= Yii::$app->controller->renderPartial("parts/works"); ?>

            <!-- Раздел Обо мне -->
            <?= Yii::$app->controller->renderPartial("parts/about"); ?>

            <!-- Раздел Клиенты -->
            <?= Yii::$app->controller->renderPartial("parts/clients"); ?>

            <!-- Раздел Контакты -->
            <?= Yii::$app->controller->renderPartial("parts/contacts"); ?>

            <!-- Контакты футер -->
            <?= Yii::$app->controller->renderPartial("menu/footer"); ?>

        </div>

        <!-- Блок частиц -->
        <div id="particles-js" class="ui four wide column right-content-side side-image-block"></div>

    </div>


    <!-- Модальное описание -->
    <div class="ui modal work-modal" data-url="<?= Url::to(["/api"]); ?>">
        <div class="ui inverted dimmer">
            <div class="ui text loader work-process"></div>
        </div>

        <div class="header work-header"></div>
        <i class="close icon"></i>
        <div class="image">
            <img class="ui centered medium image work-image" src="">
        </div>
        <div class="content">
            <div class="description">
                <div class="ui grid">
                    <div class="sixteen wide center aligned column">
                        <h3 class="header work-position"></h3>
                    </div>
                    <div class="sixteen wide column">
                        <div class="ui centered grid">
                            <div class="six wide column">
                                <p class="work-technology"></p>
                            </div>
                            <div class="six wide column">
                                <p class="work-description"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <?php $this->endBody(); ?>

    </body>
    </html>

<?php $this->endPage(); ?>