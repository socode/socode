<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>

<div id="main" class="row">
    <div class="eleven wide column">
        <div class="ui column centered grid">
            <div class="middle aligned row">

                <div class="ui grid">

                    <!-- Имя и Аватар -->
                    <div class="sixteen wide column main-header">
                        <div class="name-header">
                            <h1 class="ui header"><?= Yii::t("appMain", "about_name"); ?></h1>
                            <h2 class="ui header"><?= Yii::t("appMain", "profession"); ?></h2>
                        </div>

                        <div class="socode-divider"><?= Html::encode("</>"); ?></div>
                        <img class="ui small centered circular image main-avatar"
                             src="<?= Url::to(["img/avatar.png"]); ?>">
                        <div class="socode-divider"><?= Html::encode("</>"); ?></div>
                    </div>

                    <!-- Призыв к действию на главной -->
                    <div class="sixteen wide column">
                        <div class="ui secondary large vertical animated button call-to-action write-me-btn"
                             tabindex="0">
                            <div class="visible content"><?= Yii::t("appMain", "write"); ?></div>
                            <div class="hidden content">
                                <i class="down arrow icon"></i>
                            </div>
                        </div>
                        <a class="ui inverted red large animated fade button call-to-action"
                           tabindex="0"
                           href="<?= Url::to(["pdf/resume_ru.pdf"]); ?>" target="_blank">
                            <div class="visible content">PDF</div>
                            <div class="hidden content">
                                <i class="cloud download icon"></i>
                            </div>
                        </a>
                    </div>

                    <!-- Раздел соц. сетей -->
                    <div class="sixteen wide column">
                        <a class="ui circular linkedin icon button" href="//www.linkedin.com/in/socode/"
                           target="_blank">
                            <i class="linkedin icon"></i>
                        </a>
                        <a class="ui circular white icon button" href="//moikrug.ru/socode"
                           target="_blank">
                            <b>M</b>
                        </a>
                        <a class="ui circular blue icon button" href="//bitbucket.org/socode/"
                           target="_blank">
                            <i class="bitbucket icon"></i>
                        </a>
                    </div>

                    <!-- Анимация прокрутки мыши -->
                    <div class="sixteen wide column">
                        <div class="row">
                            <div class="scroll-downs">
                                <div class="mousey">
                                    <div class="scroller"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<div class="ui divider"></div>