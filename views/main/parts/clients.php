<div id="clients" class="row">
    <div class="ui center aligned grid">

        <div class="eleven wide center aligned column main-header">
            <h1 class="ui header"><?= Yii::t("appMainMenu", "clients"); ?></h1>
        </div>

        <div class="eleven wide center aligned column">
            <div class="work-with-container custom-container">
                <ul class="work-with-ul">
                    <a href="//www.tyazhmash.com" target="_blank" title="JSC Tyazhmash" rel="details"
                       itemprop="url">
                        <li class="tm" itemprop="image"></li>
                    </a>
                    <a href="//www.artcream.ru" target="_blank" title="Artcream" rel="details" itemprop="url">
                        <li class="ac" itemprop="image"></li>
                    </a>
                    <a href="//www.xcraft.ru" target="_blank" title="Xcraft Online Game" rel="details"
                       itemprop="url">
                        <li class="xc" itemprop="image"></li>
                    </a>
                    <a href="//www.infogis.kz" target="_blank" title="Infogis navigation and marketing"
                       rel="details" itemprop="url">
                        <li class="ig" itemprop="image"></li>
                    </a>
                    <a href="//www.mann-ivanov-ferber.ru/" target="_blank"
                       title="MIF - Innovative business books publisher" rel="details" itemprop="url">
                        <li class="mif" itemprop="image"></li>
                    </a>
                    <a href="//www.noosphere.education" target="_blank" title="Noosphere education"
                       rel="details" itemprop="url">
                        <li class="noo" itemprop="image"></li>
                    </a>
                </ul>
            </div>
        </div>

        <div class="eleven wide center aligned column">
            ← <i class="icon wheelchair"></i> →
        </div>

    </div>
</div>

<div class="ui divider"></div>