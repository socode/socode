<?php
use yii\helpers\Url;
?>

<div id="contacts" class="row">

    <div class="ui centered grid">
        <div class="eleven wide center aligned column main-header">
            <h1 class="ui header"><?= Yii::t("appMainMenu", "contacts"); ?></h1>
        </div>

        <div class="eleven wide column">
            <form id="write-me-form" class="ui form" action="<?= Url::to("contact/email"); ?>">
                <div class="field">
                    <label><?= Yii::t("appContactForm", "name"); ?> *</label>
                    <input id="contact-name" type="text" name="name" required min="3">
                </div>
                <div class="field">
                    <label><?= Yii::t("appContactForm", "phone"); ?></label>
                    <input id="contact-phone" type="tel" name="phone">
                </div>
                <div class="field">
                    <label><?= Yii::t("appContactForm", "email"); ?> *</label>
                    <input id="contact-email" type="text" name="email" required>
                </div>
                <div class="field">
                    <label><?= Yii::t("appContactForm", "message"); ?> *</label>
                    <textarea id="contact-message" name="message" required minlength="10"></textarea>
                </div>
                <input id="csrf-contact" type="hidden" name="_csrf" value="">
                <button id="send-contact" class="ui inverted green animated fade button call-to-action"
                        type="submit">
                    <div class="visible content"><?= Yii::t("appContactForm", "write"); ?></div>
                    <div class="hidden content">
                        <i class="send icon"></i>
                    </div>
                </button>
            </form>
        </div>
    </div>

</div>

<div class="ui divider"></div>