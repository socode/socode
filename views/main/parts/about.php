<div id="about" class="row">
    <div class="ui center aligned grid">

        <div class="eleven wide center aligned column main-header">
            <h1 class="ui header"><?= Yii::t("appMainMenu", "about"); ?></h1>
        </div>

        <div class="eleven wide center aligned column">
            <div class="ui left aligned stackable grid about-text">
                <div class="eight wide column">
                    <p><?= Yii::t("appAbout", "about_part_one"); ?></p>
                </div>
                <div class="eight wide column">
                    <p><?= Yii::t("appAbout", "about_part_two"); ?></p>
                </div>
            </div>

        </div>

    </div>
</div>
<!-- Призыв к действию About -->
<div class="row call-to-container">
    <div class="ui grid">
        <div class="center aligned column">
            <div id="write-me-btn" class="ui green large vertical animated button call-to-action write-me-btn"
                 tabindex="0">
                <div class="visible content"><?= Yii::t("appMain", "write"); ?></div>
                <div class="hidden content">
                    <i class="down arrow icon"></i>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="ui divider"></div>