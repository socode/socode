<?php
use yii\helpers\Url;
$_HT
?>

<div id="works" class="row">
    <div class="ui centered grid">

        <div class="sixteen wide center aligned column main-header">
            <div class="name-header">
                <h1 class="ui header"><?= Yii::t("appMainMenu", "works"); ?></h1>
            </div>
        </div>

        <div class="stackable ten wide mobile fourteen wide desctop column">
            <div class="ui link cards">

                <div class="card work-card" data-work-id="1">
                    <div class="image">
                        <img src="<?= Url::to(["img/works/tm.png"]); ?>">
                    </div>
                    <div class="content">
                        <div class="header"><?= Yii::t("appWorks", "tm"); ?></div>
                        <div class="meta">
                            <span><?= Yii::t("appWorks", "tm_position"); ?></span>
                        </div>
                        <div class="description">
                            <?= Yii::t("appWorks", "tm_description"); ?>
                        </div>
                    </div>
                </div>

                <div class="card work-card" data-work-id="2">
                    <div class="image">
                        <img src="<?= Url::to(["img/works/zobr.png"]); ?>">
                    </div>
                    <div class="content">
                        <div class="header"><?= Yii::t("appWorks", "ac"); ?></div>
                        <div class="meta">
                            <span><?= Yii::t("appWorks", "ac_position"); ?></span>
                        </div>
                        <div class="description">
                            <?= Yii::t("appWorks", "ac_description"); ?>
                        </div>
                    </div>
                </div>

                <div class="card work-card" data-work-id="3">
                    <div class="image">
                        <img src="<?= Url::to(["img/works/xcraft.png"]); ?>">
                    </div>
                    <div class="content">
                        <div class="header"><?= Yii::t("appWorks", "xc"); ?></div>
                        <div class="meta">
                            <span><?= Yii::t("appWorks", "xc_position"); ?></span>
                        </div>
                        <div class="description">
                            <?= Yii::t("appWorks", "xc_description"); ?>
                        </div>
                    </div>
                </div>

                <div class="card work-card" data-work-id="4">
                    <div class="image">
                        <img src="<?= Url::to(["img/works/infogis.png"]); ?>">
                    </div>
                    <div class="content">
                        <div class="header"><?= Yii::t("appWorks", "inf"); ?></div>
                        <div class="meta">
                            <span><?= Yii::t("appWorks", "inf_position"); ?></span>
                        </div>
                        <div class="description">
                            <?= Yii::t("appWorks", "inf_description"); ?>
                        </div>
                    </div>
                </div>

                <div class="card work-card" data-work-id="5">
                    <div class="image">
                        <img src="<?= Url::to(["img/works/mif.png"]); ?>">
                    </div>
                    <div class="content">
                        <div class="header"><?= Yii::t("appWorks", "mif"); ?></div>
                        <div class="meta">
                            <span><?= Yii::t("appWorks", "mif_position"); ?></span>
                        </div>
                        <div class="description">
                            <?= Yii::t("appWorks", "mif_description"); ?>
                        </div>
                    </div>
                </div>

                <div class="card work-card" data-work-id="6">
                    <div class="image">
                        <img src="<?= Url::to(["img/works/noo.png"]); ?>">
                    </div>
                    <div class="content">
                        <div class="header"><?= Yii::t("appWorks", "noo"); ?></div>
                        <div class="meta">
                            <span><?= Yii::t("appWorks", "noo_position"); ?></span>
                        </div>
                        <div class="description">
                            <?= Yii::t("appWorks", "noo_description"); ?>
                        </div>
                    </div>
                </div>

                <div class="card work-card" data-work-id="7">
                    <div class="image">
                        <img src="<?= Url::to(["img/works/artec.png"]); ?>">
                    </div>
                    <div class="content">
                        <div class="header"><?= Yii::t("appWorks", "artec"); ?></div>
                        <div class="meta">
                            <span><?= Yii::t("appWorks", "artec_position"); ?></span>
                        </div>
                        <div class="description">
                            <?= Yii::t("appWorks", "artec_description"); ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>

<div class="ui divider"></div>
