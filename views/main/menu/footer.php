<div class="row">
    <div class="ui container">
        <div class="ui three equal width column grid" itemscope itemtype="http://schema.org/Person">
            <div class="left aligned column">
                <a href="tel:+79277885504" title="+7 927 788-55-04" itemprop="telephone">+7 927 788-55-04</a>
            </div>
            <div class="center aligned column">
                © 2012–<?= date("Y"); ?>
                <a href="skype:art.socode" itemprop="url"><i class="icon skype"></i>Art.Socode</a>
                <br/>
                <b itemprop="name">Stanislav Zubin</b>
            </div>
            <div class="right aligned column">
                <a href="mailto:info@socode.ru" title="info@socode.ru" itemprop="email">
                    <?= Yii::$app->params["info_email"]; ?>
                </a>
            </div>
        </div>
    </div>
</div>