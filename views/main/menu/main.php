<?php
use yii\helpers\Url;
use app\widgets\WLang;
?>

<div class="row">
    <div class="sixteen wide column">
        <div id="sticky-menu" class="ui top secondary huge menu main-top-menu">

            <!-- Логотип -->
            <a class="item image" href="<?= Url::to(["/"]); ?>">
                <img class="ui image" width="57" src="<?= Url::to(["img/icon/ico114.png"]); ?>">
            </a>

            <!-- Меню -->
            <a class="item active-navigation" data-scrollto="#main"><?= Yii::t("appMainMenu", "main"); ?></a>
            <a class="item active-navigation" data-scrollto="#works"><?= Yii::t("appMainMenu", "works"); ?></a>
            <a class="item active-navigation" data-scrollto="#about"><?= Yii::t("appMainMenu", "about"); ?></a>
            <a class="item active-navigation"
               data-scrollto="#clients"><?= Yii::t("appMainMenu", "clients"); ?></a>
            <a class="item active-navigation"
               data-scrollto="#contacts"><?= Yii::t("appMainMenu", "contacts"); ?></a>

            <!-- Скрытое меню для моб. девайсов -->
            <div id="mobile-menu-btn" class="ui right secondary menu grid">
                <div class="tablet only middle aligned sixteen wide column">
                    <div class="ui large green button"><?= Yii::t("appMainMenu", "menu"); ?></div>
                </div>
                <div class="mobile only middle aligned sixteen wide column">
                    <div class="ui large centered green button"><i class="content icon zero-margin"></i></div>
                </div>
            </div>

            <!-- Виджет переключения языка -->
            <div class="right menu">
                <?= WLang::widget(); ?>
            </div>

        </div>
    </div>
</div>