<div class="ui left vertical menu sidebar">
    <a class="item sidebar-active-navigation"
       data-scrollat=".main1"><?= Yii::t("appMainMenu", "main"); ?></a>
    <a class="item sidebar-active-navigation"
       data-scrollat=".works1"><?= Yii::t("appMainMenu", "works"); ?></a>
    <a class="item sidebar-active-navigation"
       data-scrollat=".clients1"><?= Yii::t("appMainMenu", "clients"); ?></a>
    <a class="item sidebar-active-navigation"
       data-scrollat=".contacts1"><?= Yii::t("appMainMenu", "contacts"); ?></a>
</div>