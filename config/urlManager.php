<?php

return [
    "enablePrettyUrl" => true,
    "showScriptName" => false,
    "enableStrictParsing" => true,
    "class" => "codemix\\localeurls\\UrlManager",

    "enableDefaultLanguageUrlCode" => true,
    "languages" => [
        "ru" => "ru-RU",
        "en" => "en-US",
    ],

    // URL которые будут игнорировать язык
    "ignoreLanguageUrlPatterns" => [
        //"#^api#" => "#^api#", // API сервера
        "#^img/#" => "#^img/#", // Картинки с сервера
        "#^js/#" => "#^js/#", // Json с сервера
        "#^pdf/#" => "#^pdf/#", // Json с сервера
        "#^contact/#" => "#^contact/#", // Форма отправки email
    ],

    "rules" => [
        [
            "class" => "yii\\rest\\UrlRule",
            "controller" => "api",
            "except" => ["delete", "create", "update"],
        ],

        // Main контроллер
        "" => "main/index",
        "contact/email" => "main/email",

        "api" => "api/works",
    ],
];