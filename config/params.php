<?php

return [
    'admin_email' => 'admin@socode.ru',
    'support_email' => 'support@socode.ru',
    'pay_email' => 'pay@socode.ru',
    'registration_email' => 'registration@socode.ru',
    'info_email' => 'info@socode.ru',
    'cookieExpire' => time() + 86400 * 365, //1 год
    'cookieExpireWeek' => time() + 86400 * 7, //Неделя
];
