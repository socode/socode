<?php

return [
    //Переопределение бандла с Jquery на Google CDN
    'bundles' => [
        'yii\web\JqueryAsset' => [
            'sourcePath' => null,
            'js' => [
                YII_ENV_DEV ?
                    '//ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.js' :
                    '//ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js'
            ]
        ],
        // Разрешение конфликта материалайз модуля tab и jquery ui модуля tab
        // min.js с отключённым модулем tabs
        'yii\jui\JuiAsset' => [
            'sourcePath' => '@web',
            'css' => [],
            'js' => [
                'js/jquery-ui.min.js'
            ]
        ],
    ],
    //Конвертер для препроцессора Sass, Less, Scss
    'converter' => require(__DIR__ . '/preprocConv.php'),
    'linkAssets' => true,
    'appendTimestamp' => true,
];