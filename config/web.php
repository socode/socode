<?php

/**
 * Куски конфига
 * ======================================================
 */
$i18n = require_once (__DIR__ . '/i18n.php');
$assetManager = require_once (__DIR__ . '/assertManager.php');
$mailer = require_once (__DIR__ . '/mailer.php');
$log = require_once (__DIR__ . '/log.php');
$db = require_once (__DIR__ . '/db.php');
$urlManager = require_once (__DIR__ . '/urlManager.php');
$params = require_once (__DIR__ . '/params.php');
/**
 * =======================================================
 * =======================================================
 */

$config = [
    'id' => 'Socode.ru',
    'name' => 'Socode',
    'timeZone' => 'UTC',
    'basePath' => dirname(__DIR__),
    'language' => 'ru-RU',
    'bootstrap' => [],
    'defaultRoute' => 'main/index',
    'components' => [
        'request' => [
            'cookieValidationKey' => 'B_8CCofIZZv-FOb093woGO6_KvvWWwuC',
        ],
        'cache' => [
            'class' => 'yii\caching\ApcCache',
            'useApcu' => true,
        ],
        'formatter' => [
            'dateFormat' => 'd.MM.yyyy',
            'timeFormat' => 'H:mm:ss',
            'datetimeFormat' => 'd.MM.yyyy H:mm',
        ],
        'i18n' => $i18n, //PHPIntl - 52.1
        'assetManager' => $assetManager,
        'user' => [
            'identityClass' => 'app\models\Users',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'main/error',
        ],
        'mail' => $mailer,
        'log' => $log,
        //'db' => $db,
        'urlManager' => $urlManager,
    ],
    'params' => $params,
];

if (YII_DEBUG) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug']['class'] = 'yii\debug\Module';
    $config['modules']['debug']['allowedIPs'] = ['*'];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii']['class'] = 'yii\gii\Module';
    $config['modules']['gii']['allowedIPs'] = ['*'];
}
return $config;
