<?php
return [
    'translations' => [
        'app*' => [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en',
        ],
        'yii' => [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@app/messages'
        ],
    ],
];