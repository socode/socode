<?php

return [
    'class' => 'nizsheanez\assetConverter\Converter',
    'force' => true, // true : Если хотите конвертировать SASS каждый раз без задержки
    'destinationDir' => 'css/compiled', // Директория в @webroot для результата
    'parsers' => [
        'sass' => [
            'class' => 'nizsheanez\assetConverter\Sass',
            'output' => 'css', // Тип выходного файла
            'options' => [
                'cachePath' => '@app/runtime/cache/sass-parser' // Выборочные опции
            ],
        ],
        'scss' => [
            'class' => 'nizsheanez\assetConverter\Sass',
            'output' => 'css',
            'options' => []
        ],
        'less' => [
            'class' => 'nizsheanez\assetConverter\Less',
            'output' => 'css',
            'options' => [
                'auto' => true,
            ]
        ]
    ]
];