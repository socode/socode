<?php

namespace app\models;

use Yii;

class Works
{

    /**
     * @param $id
     * @return mixed
     *
     * Возвращает информацию о работе по ID в массиве
     */
    public static function getWorkInfo($id)
    {
        $work_list = Yii::t("appWorks", "works");
        if ((int)$id > 0 && $id <= count($work_list)) {
            return $work_list[$id];
        } else {
            return false;
        }
    }
}