<?php
namespace app\models;

use Yii;
use yii\helpers\Html;

class Email {


    protected $email = "";
    protected $data = [];
    protected $error_message = [];

    public function __construct($data = [])
    {
        $this->loadData($data);
    }

    /**
     * @return array
     * Возвращает данные для отправки
     */
    protected function getData()
    {
        return $this->data;
    }

    /**
     * @param $data
     * @return mixed
     * Загружает данные для отправки
     */
    protected function loadData($data)
    {
       return $this->data = $data;
    }

    /**
     * @param $data
     * Задаёт сообщение ошибки
     */
    private function setError($data)
    {
        $this->error_message = $data;
    }

    /**
     * @return array
     * Возвращает сообщение об ошибке
     */
    public function getError()
    {
        return $this->error_message;
    }

    /**
     * @return array|bool
     * Проверяет данные для отправки
     */
    public function checkData()
    {
        $data = $this->getData();
        if (!empty($data)) {
            if (!empty($data["email"]) && filter_var($data["email"], FILTER_VALIDATE_EMAIL)) {
                return true;
            } else {
                $error = [
                    "status" => 500,
                    "message" => "Не правельный email"
                ];
                $this->setError($error);
                return false;

            }
        } else {
            $error = [
                "status" => 500,
                "message" => "Даннеые не могут быть пустыми"
            ];
            $this->setError($error);
            return false;
        }

    }

    /**
     * @param $data
     * @return array
     * Подготавливает данные для отправки
     */
    protected function prepareData($data)
    {
        return $new_data = [
            "name" => Html::encode($data["name"]),
            "phone" => Html::encode($data["phone"]),
            "email" => Html::encode($data["email"]),
            "message" => Html::encode($data["message"])
        ];
    }

    /**
     * @param string $data
     * @return array|bool
     * Отправляет письмо с сообщением от пользователя сайта
     */
    public function sendEmail($data = "")
    {
        $new_data = $this->getData();

        if (!empty($data)) {
            $this->loadData($data);
            $new_data = $this->getData();
        }

        if (!$this->checkData()) {
            return $this->getError();
        }

        $new_data = $this->prepareData($new_data);

        return Yii::$app->mail->compose('contact_form/contact', ["data" => $new_data])
            ->setFrom(Yii::$app->params['admin_email'])
            ->setTo(Yii::$app->params['info_email'])
            ->setSubject('Письмо с сайта!')
            ->send();
    }
}
