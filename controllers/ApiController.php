<?php

namespace app\controllers;

use app\models\Works;
use Yii;
use yii\web\Response;
use yii\rest\ActiveController;


class ApiController extends ActiveController
{
    public function init()
    {
        $this->modelClass = "";
        parent::init();
    }

    public function actions()
    {
        $actions = parent::actions();
    }

    /**
     * @param $id
     * @return array
     *
     * Возвращает информацию о работе по ID
     */
    public function actionWorks($id)
    {
        /*Приводим ответ к JSON типу*/
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (!empty($id)) {
            $work_text = Works::getWorkInfo($id);

            if ($work_text === false) {
                return [
                    "status" => 404,
                    "message" => "Нет верный ID"
                ];
            } else {
                return [
                    "status" => 200,
                    "work" => $work_text,
                    "work_process" => Yii::t("appWorks", "work_process"),
                    "message" => "Ок"
                ];
            }
        } else {
            return [
                "status" => 404,
                "message" => "ID не указан"
            ];
        }
    }
}