<?php

namespace app\controllers;

use app\models\Email;
use yii\web\Controller;

class MainController extends Controller
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * @return string
     *
     * Главная страница
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string
     *
     * Главная страница
     */
    public function actionEmail()
    {
        $data = \Yii::$app->request->post();
        //$mail = new Email($data);
        //$mail->sendEmail();
    }

}
