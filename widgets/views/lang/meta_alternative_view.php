<?php foreach (Yii::$app->urlManager->languages as $lang_key => $value): ?>
    <?php if (Yii::$app->language === $value): continue; ?>
    <?php else: ?>
        <link rel="alternate" hreflang="<?= $lang_key; ?>" href="<?= Yii::$app->request->hostInfo . "/" . $lang_key; ?>" />
    <?php endif; ?>
<?php endforeach; ?>