<?php
use yii\helpers\Html;
use yii\helpers\Url;

foreach ($langs as $lang) {
    if ( $lang["current"] ) {
        $current_lang = $lang["label"];
    }
}

$get_param = Yii::$app->request->queryString; // var=value
$pathInfo = Yii::$app->request->pathInfo; // URI

$url = "/" . $pathInfo;
$url .= $get_param === "" ? "" : "?" . $get_param;
?>

<div class="ui right grid">

    <div class="computer only middle aligned sixteen wide column">
        <div class="ui dropdown item">
            <i class="world icon"></i><?= $current_lang; ?><i class="dropdown icon"></i> <div class="menu">
                <?php foreach ($langs as $lang):?>
                    <?php $active = !is_null($lang["current"]) ? 'active' : ''; ?>
                    <?= Html::a(
                        $lang["label_flag"],
                        Url::to( "/" . $lang["url"]["language"] . $url),
                        ["class" => "item $active"]
                    ); ?>
                <?php endforeach;?>
            </div>
        </div>
    </div>
    <div class="tablet only mobile only middle aligned sixteen wide column">
        <div class="ui dropdown item">
            <i class="world icon"></i><i class="dropdown icon"></i> <div class="menu">
                <?php foreach ($langs as $lang):?>
                    <?php $active = !is_null($lang["current"]) ? 'active' : ''; ?>
                    <?= Html::a(
                        $lang["label_flag"],
                        Url::to( "/" . $lang["url"]["language"] . $url),
                        ["class" => "item $active"]
                    ); ?>
                <?php endforeach;?>
            </div>
        </div>
    </div>
</div>