<?php
namespace app\widgets;

use yii;
use yii\base\Widget;

/**
 * Class WLang
 * @package app\widgets
 */
class WLang extends Widget
{
    public $meta_alt_only = false;
    private static $_labels;
    private static $_labels_with_flag;
    private $_lang = [];

    /**
     * Обрезает язык тип ru-RU, en-US до конструкции ru, en если необходимо.
     * Так-же возвращает меню со списком языков, ссылками и переводом.
     * 
     * Если передан флаг meta_alt_only, то формирует только теги альтернативных языков 
     */
    public function init()
    {
        $route = Yii::$app->controller->route === Yii::$app->defaultRoute ? "" : Yii::$app->controller->route;
        $appLanguage = Yii::$app->language;
        $params = $_GET;

        array_unshift($params, "/".$route);

        foreach (Yii::$app->urlManager->languages as $language) {

            // Проверяем язык на наличие -* (en-*, etc.)
            $isWildcard = substr($language, -2)==="-*";
            $current = null;

            if (
                // Если язык совпадают, то продолжить перебор списка языков
                $language === $appLanguage ||
                $isWildcard && substr($appLanguage,0,2) === substr($language,0,2)
            ) {
                $current = true;
                $language = substr($language,0,2);
                //continue;   // Т.о. исключаем текущий язык сайта
            }

            if ( $isWildcard ) {
                $language = substr($language,0,2);
            }

            $params["language"] = substr($language,0,2);
            $this->_lang[] = [
                "label" => self::label($language),
                "label_flag" => self::label_flag($language),
                "current" => $current,
                "url" => $params,
            ];
        }

        parent::init();
    }


    /**
     * @param $code
     * @return mixed|null
     *
     * Возвращает перевод языка
     */
    public static function label($code)
    {
        if ( self::$_labels === null) {

            self::$_labels = [
                "ru" => Yii::t("appLang", "Russian"),
                "en" => Yii::t("appLang", "English"),
                "ru-RU" => Yii::t("appLang", "Russian"),
                "en-US" => Yii::t("appLang", "English"),
            ];

        }

        return isset(self::$_labels[$code]) ? self::$_labels[$code] : null;

    }

    public static function label_flag($code)
    {
        if ( self::$_labels_with_flag === null) {

            self::$_labels_with_flag = [
                "ru" => "<i class=\"russia flag\"></i>" . Yii::t("appLang", "Russian"),
                "en" => "<i class=\"united states flag\"></i>" . Yii::t("appLang", "English"),
                "ru-RU" => "<i class=\"russia flag\"></i>" . Yii::t("appLang", "Russian"),
                "en-US" => "<i class=\"united states flag\"></i>" . Yii::t("appLang", "English"),
            ];
        }

        return isset(self::$_labels_with_flag[$code]) ? self::$_labels_with_flag[$code] : null;

    }

    public function run()
    {
        if ($this->meta_alt_only) {
            return $this->render("lang/meta_alternative_view");
        } else {
            return $this->render("lang/view", [
                "langs" => $this->_lang,
            ]);
        }
    }

}