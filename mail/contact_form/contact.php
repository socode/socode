<?php
use yii\helpers\Html;
?>

<p>Письмо с сайта <b><?= Html::encode(Yii::$app->request->hostInfo); ?></b></p>
<p>Данные:</p>
<ul>
    <li>Имя: <?= Html::encode($data["name"]); ?></li>
    <li>Номер: <?= Html::encode($data["phone"]); ?></li>
    <li>Email: <?= Html::encode($data["email"]); ?></li>
</ul>
<p>Сообщение:</p><br>
<div style="width: 300px !important;">
    <?= Html::encode($data["message"]); ?>
</div>